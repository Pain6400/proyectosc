﻿using System.Web;
using System.Web.Optimization;

namespace GSC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/js").Include(
                      "~/Content/assets/vendors/js/vendor.bundle.base.js",
                      "~/Content/assets/ vendors/chart.js/Chart.min.js",
                      "~/Content/assets/js/off-canvas.js",
                      "~/Content/assets/js/hoverable-collapse.js",
                      "~/Content/assets/js/misc.js",
                      "~/Content/assets/js/dashboard.js",
                      "~/Content/assets/js/todolist.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/assets/vendors/mdi/css/materialdesignicons.min.css",
                        "~/Content/assets/vendors/css/vendor.bundle.base.css",
                        "~/Content/assets/css/style.css"));
        }
    }
}
