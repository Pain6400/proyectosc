﻿using BaseDeDatos;
using BaseDeDatos.Tikets;
using GSC.Models.Tikets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSC.Controllers
{
    public class TiketsController : Controller
    {
        TiketsData data = new TiketsData();
        // GET: Tikets
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CrearTiket()
        {
            var model = new CrearTiketsViewModel
            {
                listaPrioridad = this.GetTiposprioridad()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CrearTiket(CrearTiketsViewModel model)
        {
            var guarda = new Tickets
            {
                Usuario = model.Usuario,
                Email = model.Email,
                IdPreoridad = model.IdPreoridad,
                Telefono = model.Telefono,
                Problema = model.Problema,
                Descripcion = model.Descripcion,
                Fecha_Creacion = DateTime.Now,
                IdEstado = 1
            };

            data.GuardarTiket(guarda);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Gestionar()
        {
            var model = data.GetTikets().Select(s => new CrearTiketsViewModel
            {
                ID = s.ID,
                Usuario = s.Usuario,
                Email = s.Email,
                Problema = s.Problema,
                PrioridadNombre = s.Prioridad.Nombre
            }).ToList();
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult GestionarTiket(int id)
        {
            var infomacion = data.GetTiketPorId(id);
            var model = new CrearTiketsViewModel
            {
                Usuario = infomacion.Usuario,
                Email = infomacion.Email,
                IdPreoridad = infomacion.IdPreoridad,
                Telefono = infomacion.Telefono,
                Problema = infomacion.Problema,
                Descripcion = infomacion.Descripcion,
                Fecha_Creacion = infomacion.Fecha_Creacion,
                listaPrioridad = this.GetTiposprioridad(),
                listaEquipo = this.GetTDepartamentos()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult GestionarTiket(CrearTiketsViewModel model)
        {
            var guardar = data.GetTiketPorId(model.ID);
            guardar.Usuario = model.Usuario;
            guardar.Email = model.Email;
            //guardar.IdPreoridad = model.IdPreoridad;
            guardar.Telefono = model.Telefono;
            guardar.Problema = model.Problema;
            guardar.Descripcion = model.Descripcion;
            guardar.Fecha_Entrega = model.Fecha_Entrega;
            guardar.IdEstado = 2;
            guardar.IdEquipoDepartamento = model.IdEquipoDepartamento;
            data.ActualizarTiket(guardar);
            return RedirectToAction("Gestionar");
        }

        [Authorize]
        public ActionResult ResolverTikets()
        {
            var model = data.GetTiketsEnProceso().Select(s => new CrearTiketsViewModel
            {
                ID = s.ID,
                Usuario = s.Usuario,
                Email = s.Email,
                Problema = s.Problema,
                PrioridadNombre = s.Prioridad.Nombre,
                Fecha_Entrega = s.Fecha_Entrega
            }).ToList();

            return View(model);
        }

        [Authorize]
        public ActionResult FinalizarTiket(int id)
        {
            var infomacion = data.GetTiketPorId(id);
            var model = new CrearTiketsViewModel
            {
                Usuario = infomacion.Usuario,
                Email = infomacion.Email,
                IdPreoridad = infomacion.IdPreoridad,
                Telefono = infomacion.Telefono,
                Problema = infomacion.Problema,
                Descripcion = infomacion.Descripcion,
                Fecha_Creacion = infomacion.Fecha_Creacion,
                Fecha_Entrega = infomacion.Fecha_Entrega,
            };

            return View(model);
        }

        [Authorize]
        public ActionResult EstadoTiketsTable()
        {
            var model = data.VerTiketsEstados().Select(s => new CrearTiketsViewModel
            {
                ID = s.ID,
                Usuario = s.Usuario,
                Email = s.Email,
                Problema = s.Problema,
                PrioridadNombre = s.Prioridad.Nombre,
                Fecha_Entrega = s.Fecha_Entrega,
                EstadoNombre = s.Estados.Nombre
            }).ToList();

            return View(model);
        }

        [Authorize]
        public ActionResult EstadoTiket(int id)
        {
            var infomacion = data.GetTiketConNotasPorId(id);
            var model = new CrearTiketsViewModel
            {
                Usuario = infomacion.Usuario,
                Email = infomacion.Email,
                IdPreoridad = infomacion.IdPreoridad,
                Telefono = infomacion.Telefono,
                Problema = infomacion.Problema,
                Descripcion = infomacion.Descripcion,
                Fecha_Creacion = infomacion.Fecha_Creacion,
                Fecha_Entrega = infomacion.Fecha_Entrega,
                Notas = infomacion.Notas.Select(s => new ListaNotasViewModel
                {
                    ID = s.ID,
                    IdTicket = s.IdTicket,
                    Nombre = s.Nombre,
                    Descripcion = s.Descripcion
                }).ToList()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult EstadoTiket(CrearTiketsViewModel model)
        {
            var nota = new Notas
            {
                IdTicket = model.ID,
                Nombre = model.NombreNota,
                Descripcion = model.DescripcionNota
            };

            data.GuardarNota(nota);

            return RedirectToAction("EstadoTiket", new { model.ID });
        }

        [HttpPost]
        [Authorize]
        public ActionResult FinalizarTiket(CrearTiketsViewModel model)
        {
            var finalizar = data.GetTiketPorId(model.ID);
            finalizar.IdEstado = 4;
            data.FinalizarTiket(finalizar);
            return RedirectToAction("ResolverTikets");
        }


        private SelectList GetTiposprioridad()
        {
            return new SelectList(data.GetPrioridadList(), nameof(Prioridad.ID), nameof(Prioridad.Nombre));
        }

        private List<SelectListItem> GetTDepartamentos()
        {
            var model = data.GetDepartamentosList().Select(s => new SelectListItem 
            {
                Value = s.ID.ToString(),
                 Text = string.Format("{0} - {1}", s.Nombre, s.Departamentos.Nombre)
            }).ToList();

            return model;
        }
    }
}