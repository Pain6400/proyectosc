﻿using BaseDeDatos.ProveedoresFolder;
using System.Web.Mvc;

namespace GSC.Controllers
{
    [Authorize]
    public class ProveedoresController : Controller
    {
        ProveedoresData data = new ProveedoresData(); 
        // GET: Proveedores
        public ActionResult Index()
        {
            return View(data.ListaDeProveedores());
        }
    }
}