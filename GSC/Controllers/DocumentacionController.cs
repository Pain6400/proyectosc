﻿using BaseDeDatos;
using BaseDeDatos.DocumentacionFolder;
using GSC.Models;
using GSC.Models.Documentacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSC.Controllers
{
    [Authorize]
    public class DocumentacionController : Controller
    {
        DocumentacionData data = new DocumentacionData();
        // GET: Documentacion
        public ActionResult Index()
        {
            var model = data.GetDocumentaciones().Select(s => new ListaDocumentacionViewModel
            {
                Id = s.Id,
                Nombre = s.Nombre,
                Tipo = s.Tipo_Documentacion.Nombre
            }).ToList();

            return View(model);
        }

        public ActionResult Crear()
        {
            var model = new CrearDocumentacion
            {
                TipoLisya = this.GetTiposDocumentacion()
            };

            return View(model);
        }

        private SelectList GetTiposDocumentacion()
        {
            return new SelectList(data.GetTipoDocumentacion(), nameof(Tipo_Documentacion.TipoDocumentacion), nameof(Tipo_Documentacion.Nombre));
        }
    }
}