﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GSC.Startup))]
namespace GSC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
