﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSC.Models.Documentacion
{
    public class CrearDocumentacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public int TipoDocumentacionId { get; set; }

        public SelectList TipoLisya { get; set; }
        public string DrawIO { get; set; }
    }
}