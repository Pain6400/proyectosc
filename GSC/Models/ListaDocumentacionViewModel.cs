﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSC.Models
{
    public class ListaDocumentacionViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public string Departamento { get; set; }
        public string Tipo { get; set; }
    }
}