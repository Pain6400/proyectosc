﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSC.Models.Tikets
{
    public class ListaNotasViewModel
    {
        public int? ID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? IdTicket { get; set; }
    }
}