﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSC.Models.Tikets
{
    public class CrearTiketsViewModel
    {
        public int ID { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }
        public int Telefono { get; set; }
        public string Departamento { get; set; }
        public string Problema { get; set; }
        public string Descripcion { get; set; }
        public string Resolucion { get; set; }
        public int IdEquipoDepartamento { get; set; }
        public int IdEstado { get; set; }
        public string EstadoNombre { get; set; }
        public int IdPreoridad { get; set; }

        public string NombreNota { get; set; }

        public string DescripcionNota { get; set; }

        public string PrioridadNombre { get; set; }
        public System.DateTime Fecha_Creacion { get; set; }
        public System.DateTime? Fecha_Entrega { get; set; }

        public SelectList listaPrioridad { get; set; }
        public List<SelectListItem> listaEquipo { get; set; }

        public IEnumerable<ListaNotasViewModel> Notas { get; set; }
    }
}