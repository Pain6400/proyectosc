﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.DocumentacionFolder
{
    public class DocumentacionData
    {
        public List<Documentacion> GetDocumentaciones()
        {
            using (var db = new GSCEntities())
            {
                return db.Documentacion
                         .Include("Tipo_Documentacion")
                         .ToList();
            }
        }

        public List<Tipo_Documentacion> GetTipoDocumentacion()
        {
            using (var db = new GSCEntities())
            {
                return db.Tipo_Documentacion.ToList();
            }
        }
    }
}
