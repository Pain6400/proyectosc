﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.ProveedoresFolder
{
    public class ProveedoresData
    {
        public List<ProveedoresViewModel> ListaDeProveedores()
        {
            using (var db = new GSCEntities())
            {
                return db.Proveedores.Select(s => new ProveedoresViewModel 
                {
                    Email = s.Email,
                    Nombre = s.Nombre,
                    Telefono = s.Telefono
                }).ToList();
            }
        }
    }
}
