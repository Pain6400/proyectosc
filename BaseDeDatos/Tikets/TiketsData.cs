﻿    using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Tikets
{
    public class TiketsData
    {
        public List<Prioridad> GetPrioridadList()
        {
            using (var db = new GSCEntities())
            {
                return db.Prioridad.ToList();
            }
        }

        public List<EquipoDepartamento> GetDepartamentosList()
        {
            using (var db = new GSCEntities())
            {
                return db.EquipoDepartamento.Include("Departamentos").ToList();
            }
        }

        public List<Tickets> GetTikets()
        {
            using (var db = new GSCEntities())
            {
                return db.Tickets.Include("Prioridad").Where(w => w.IdEstado == 1).ToList();
            }
        }

        public List<Tickets> GetTiketsEnProceso()
        {
            using (var db = new GSCEntities())
            {
                return db.Tickets.Include("Prioridad").Where(w => w.IdEstado == 2).ToList();
            }
        }

        public List<Tickets> VerTiketsEstados()
        {
            using (var db = new GSCEntities())
            {
                return db.Tickets
                         .Include("Prioridad")
                         .Include("Estados")
                         .ToList();
            }
        }

        public Tickets GetTiketPorId(int id)
        {
            using (var db = new GSCEntities())
            {
                return db.Tickets
                         .Include("Prioridad")
                         .Include("EquipoDepartamento")
                         .FirstOrDefault(w => w.ID == id);
            }
        }

        public Tickets GetTiketConNotasPorId(int id)
        {
            using (var db = new GSCEntities())
            {
                return db.Tickets
                         .Include("Prioridad")
                         .Include("EquipoDepartamento")
                         .Include(s => s.Notas)
                         .FirstOrDefault(w => w.ID == id);
            }
        }
        public string GuardarTiket(Tickets model)
        {
            try
            {
                using (var db = new GSCEntities())
                {
                    db.Tickets.Add(model);
                    db.SaveChanges();
                    return "Correcto";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ActualizarTiket(Tickets model)
        {
            try
            {
                using (var db = new GSCEntities())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    return "Correcto";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string FinalizarTiket(Tickets model)
        {
            try
            {
                using (var db = new GSCEntities())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    return "Correcto";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string GuardarNota(Notas model)
        {
            try
            {
                using (var db = new GSCEntities())
                {
                    db.Notas.Add(model);
                    db.SaveChanges();
                    return "Correcto";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
